<?php

namespace crystal\core\behaviors;

use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;

use crystal\core\components\helpers\DataGenerationHelper;

/**
 * Class DatetimeBehavior
 * automatically fills the specified attributes with the current datetime value
 *
 * By default, DatetimeBehavior will fill the `created_at` and `updated_at' attributes with the current datetime when the
 * associated AR object is being inserted;
 * It will fill the `updated_at` attribute with the datetime when the AR object is being updated
 *
 * @package     crystal\core
 * @subpackage  crystal\core\behaviors
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class DatetimeBehavior extends AttributeBehavior
{
    /**
     * @var string the attribute that will receive datetime value
     * set this property to false if you do not want to record the creation time.
     */
    public $createdAtAttribute = 'created_at';

    /**
     * @var string the attribute that will receive datetime value.
     * set this property to false if you do not want to record the update time.
     */
    public $updatedAtAttribute = 'updated_at';

    /**
     * {@inheritdoc}
     * in case, when the value is `null`, the result of the Entity:currentDateTime will be used as value
     */
    public $value;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => [$this->createdAtAttribute, $this->updatedAtAttribute],
                BaseActiveRecord::EVENT_BEFORE_UPDATE => $this->updatedAtAttribute,
            ];
        }
    }

    /**
     * {@inheritdoc}
     * in case, when the [[value]] is `null`, a new datetime value will be generated
     */
    protected function getValue($event)
    {
        if ($this->value === null) {
            return DataGenerationHelper::currentDatetime();
        }
        return parent::getValue($event);
    }
}
