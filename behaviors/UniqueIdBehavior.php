<?php

namespace crystal\core\behaviors;

use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;

/**
 * Class UniqueIDBehavior
 * automatically fills the specified attributes with an unique_id generated hash and validates if the hash is not used in
 * the current entity instance table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\behaviors
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class UniqueIdBehavior extends AttributeBehavior
{
    /**
     * @var string the attribute
     */
    public $uniqueIdAttribute = 'unique_id';

    /**
     * {@inheritdoc}
     * in case, when the value is `null`, a new hash will be generated as value
     */
    public $value;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if ( empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => [$this->uniqueIdAttribute]
            ];
        }
    }

    /**
     * {@inheritdoc}
     * update the attach method to validate if the entity has the required property else we do not attach the event to the entity
     */
    public function attach( $owner )
    {
        if ( $owner->hasProperty($this->uniqueIdAttribute) ) {
            parent::attach($owner);
        }
    }

    /**
     * {@inheritdoc}
     * in case, when the [[value]] is `null`, a new hash value will be generated
     */
    protected function getValue( $event )
    {
        if ( $this->value === null ) {

            do {
                $className = $this->owner->classname();
                $uniqueId = \Yii::$app->security->generateRandomString(64);
            } while ( $this->_isUniqueId( $className, $uniqueId) === false );

            $this->value = $uniqueId;
        }

        return parent::getValue($event);
    }

    /**
     * Validates the generated `$uniqueId` if is unique in the current entity table
     * @return bool whatever the `$uniqueId` is unique or not
     */
    private function _isUniqueId( $className, $uniqueId )
    {
        if ( $className::findByUniqueId($uniqueId) === null ) {
            return true;
        }

        return false;
    }
}
