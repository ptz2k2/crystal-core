<?php

namespace crystal\core\components;

use yii\base\Event;

use crystal\core\engine\BaseComponent;
use crystal\core\controllers\Controller;

/**
 * Class MaintenanceComponent
 * implements the application maintenance mode functionality based on maintenance configuration.
 * if the maintenance mode is enabled the component will add an event listener for the Controller::BEFORE_ACTION_EVENT.
 * if the maintenance key not exists the component will check the current requested route is not the maintenance page
 * and validates if the requested route is one of the exluded routes else will redirect to maintenance page.
 * if the maintenance key exists the maintenance component will not do anything.
 *
 * The maintenance key is used by developers to negate the maintenance mode redirection.
 *
 * @property int $status the maintenance component status, will be populated on component init method
 * @property string $key the maintenance key configuration value, will be populated on component init method
 * @property string $routeIdentity the maintenance page route identity, will be populated on component init method
 * @property array $excludedRoutes the list of excluded routes, the maintenance component will not redirect to maintenance
 * page if the requested route is one of the excluded routes
 *
 * @package     crystal\core
 * @subpackage  crystal\core\components
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class MaintenanceComponent extends BaseComponent
{
    const COMPONENT_SESSION_NAME = '_crystal-mc';

    const CONFIGURATION_STATUS = 'crystal.maintenance.status';
    const CONFIGURATION_KEY = 'crystal.maintenance.key';
    const CONFIGURATION_ROUTE_IDENTITY = 'crystal.maintenance.route_identity';
    const CONFIGURATION_EXCLUDED_ROUTES = 'crystal.maintenance.excluded_routes';

    /**
     * @var int the maintenance mode status, will be populated on component init method
     */
    protected $status;

    /**
     * @var string the maintenance key, will be populated on component init method
     */
    protected $key;

    /**
     * @var string the maintenance page route identity, will be populated on component init method
     */
    protected $routeIdentity;

    /**
     * @var array the maintenance excluded routes, will be populated on component init method
     */
    protected $excludedRoutes;

    /**
     * {@inheritdoc}
     * On init method the component retrieves the component configurations and checks if the maintenance status is true
     * if true the method add's an event listener to catch the application Controller::EVENT_BEFORE_ACTION
     * @throws ConfigurationNotFoundException if the component configurations are not found
     */
    public function init()
    {
        parent::init();

        $this->getConfig(self::CONFIGURATION_STATUS, 'status');
        $this->getConfig(self::CONFIGURATION_KEY, 'key');
        $this->getConfig(self::CONFIGURATION_ROUTE_IDENTITY, 'routeIdentity');
        $this->getConfig(self::CONFIGURATION_EXCLUDED_ROUTES, 'excludedRoutes');

        if ( (bool) $this->status === true ) {
            Event::on(Controller::className(), Controller::EVENT_BEFORE_ACTION, function ($event) {
                return $this->handleMaintenanceMode($event);
            });
        }
    }

    /**
     * Validates if the maintenance maintenance key exists, if exists the method will not do anything.
     * if the maintenance key not exists the method will check the current requested page and if the page identity
     * is not one of the excluded page or is not the maintenance page will redirect to maintenance page
     */
    public function handleMaintenanceMode($event) : void
    {
        $pageIdentity = $event->sender->pageIdentity;

        if ( !$this->isKeyEnabled() ) {
            if ( !array_key_exists($pageIdentity, array_flip($this->excludedRoutes)) ) {
                $event->sender->redirect(\Yii::$app->routing->goToRoute($this->routeIdentity, []));
                \Yii::$app->end();
            }
        }
    }

    /**
     * Validates if the maintenance session exists and the key is valid or not
     * @return bool whatever the maintanance key is valid or not
     */
    public function isKeyEnabled() : bool
    {
        if ( !isset($this->session['key']) ) {
            return false;
        }

        if ( !$this->validateKey($this->session['key']) ) {
            return false;
        }

        return true;
    }

    /**
     * Returns the maintenance component status
     * @return bool whatever the maintenance component is enabled or not
     */
    public function getStatus() : bool
    {
        return (bool) $this->status;
    }

    /**
     * Returns the maintenance key value
     * @return string the maintenance key value
     */
    public function getKey() : string
    {
        return $this->key;
    }

    /**
     * Returns the maintenance page route identity
     * @return string the maintenance page route identity
     */
    public function getRouteIdentity() : string
    {
        return $this->routeIdentity;
    }

    /**
     * Returns the maintenance excluded routes
     * @return array the maintenance excluded routes data
     */
    public function getExcludedRoutes() : array
    {
        return $this->excludedRoutes;
    }

    /**
     * Validates the key by checking if the requested key is equal with the maintenance key
     * @param string $key the maintenance key value what you want to validate
     * @return bool whatever the key is valid or not
     */
    public function validateKey( string $key ) : bool
    {
        return ( $this->key === $key );
    }

    /**
     * {@inheritdoc}
     * not implementing since the component is not using cache storage
     */
    public function generateCacheData() : array{}
}