<?php

namespace crystal\core\components;

use yii\web\Session;

use crystal\core\helpers\ArrayHelper;

/**
 * Class SessionComponent
 * extends the base session component to override the get and set methods to implement encryption and decryption
 * of the session data
 *
 * @package     crystal\core
 * @subpackage  crystal\core\components
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class SessionComponent extends Session
{
    /**
     * {@inheritdoc}
     * overrides the function to decrypt the session data before returning the value
     * @param string $key the session variable name
     * @param mixed $defaultValue the default value to be returned when the session variable does not exist.
     * @return mixed the session variable value, or $defaultValue if the session variable does not exist.
     */
    public function get( $key, $defaultValue = null )
    {
        $this->open();
        return isset($_SESSION[$key]) ? $this->_decrypt($_SESSION[$key]) : $defaultValue;
    }

    /**
     * {@inheritdoc}
     * overrides the function to encrypt the session data before adding it to the session storage
     * @param string $key session variable name
     * @param mixed $value session variable value
     */
    public function set( $key, $value )
    {
        $this->open();
        $_SESSION[$key] = $this->_encrypt($value);
    }

    /**
     * Encrypt the session value
     * @param string|array $value the session variable value
     * @return string the encrypted session value
     */
    private function _encrypt( $value ) : string
    {
        if ( is_array($value) ) {
            $value = serialize($value);
        }

        return \Yii::$app->security->encryptByKey($value, \Yii::$app->security['session_key']);
    }

    /**
     * Decrypt the session value
     * @param mixed $value the encrypted session variable value
     * @return mixed the decrypted session value
     */
    private function _decrypt( $value )
    {
        if ( is_array($value) ) {
            return $value;
        }

        $decryptedData = \Yii::$app->security->decryptByKey($value, \Yii::$app->crystal['security.session_key']);

        if( ArrayHelper::isSerialized($decryptedData) ) {
            return unserialize($decryptedData);
        }

        return $decryptedData;
    }
}