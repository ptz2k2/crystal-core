<?php

namespace crystal\core\components;

use Yii;
use yii\helpers\Url;

use crystal\core\engine\BaseComponent;
use crystal\core\engine\exceptions\ConfigurationNotFoundException;
use crystal\core\engine\exceptions\RouteNotFoundException;
use crystal\core\models\repository\RoutesRepository;

/**
 * Class RoutingComponent
 * implements helper methods to retieve the application url's or to generate url's from routes.
 *
 * @package     crystal\core
 * @subpackage  crystal\core\components
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class RoutingComponent extends BaseComponent
{
    const CONFIGURATION_SUPPORT_SSL = 'crystal.routing.support_ssl';

    /**
     * @var int $supportSsl the routing support ssl configuration value, will be populated on component init method
     */
    protected $supportSsl;

    /**
     * {@inheritdoc}
     * On init method retrieves the routing component configurations
     * @throws ConfigurationNotFoundException if the component configurations are not found
     */
    public function init()
    {
        parent::init();

        $this->getConfig(self::CONFIGURATION_SUPPORT_SSL,  'supportSsl');
    }

    /**
     * Get the application base url
     * @return string the application base url value
     */
    public function getBaseUrl( $languageCode = true ) : string
    {
        $baseUrl = ( (bool) $this->supportSsl ) ? Url::base('https') . '/' : Url::base('http') . '/';

        if ( $languageCode === true ) {
            return $baseUrl . Yii::$app->translation->getCurrentLanguage() . '/';
        }

        return $baseUrl;
    }

    /**
     * Get the application theme url
     * @return string the application theme url
     */
    public function getThemeUrl() : string
    {
        return $this->getBaseUrl(false) . 'themes/' .Yii::$app->theme->getTheme() . '/';
    }

    /**
     * Get the application drive url
     * @return string the application drive url
     */
    public function getDriveUrl() : string
    {
        return $this->getBaseUrl(false) . 'drive/';
    }

    /**
     * Generates the url for the requested route based on the route identity
     * @param string $identity the route identity
     * @param array $parameters the route required parameters array
     * @param string $languageIdentity the language identity
     * @return string the generated route url
     */
    public function goToRoute( string $identity, array $parameters = array(), string $languageIdentity = null ) : string
    {
        if ( $languageIdentity === null ) {
            $languageIdentity = Yii::$app->translation->getCurrentLanguage();
        }

        $route = RoutesRepository::findByIdentityAndLanguage($identity, $languageIdentity);

        if ( !$route ) {
            throw new RouteNotFoundException(500,
                Yii::t('crystal.exceptions', 'The route was not found for {{0}} identity for language {{1}}', [
                    $identity,
                    $languageIdentity
                ])
            );
        }

        $generatedUrl = $route->url_path;

        if ( !empty($parameters) ) {
            foreach ( $parameters as $parameterName => $parameterValue ) :
                $generatedUrl = str_replace('{' . $parameterName . '}', $parameterValue, $generatedUrl);
            endforeach;;
        }

        return $this->getBaseUrl()  . $generatedUrl;
    }

    /**
     * {@inheritdoc}
     * not implementing since the component is not using cache storage
     */
    public function generateCacheData(): array{}
}