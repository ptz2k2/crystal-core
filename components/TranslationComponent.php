<?php

namespace crystal\core\components;

use crystal\core\engine\BaseComponent;
use crystal\core\engine\exceptions\ConfigurationInvalidException;
use crystal\core\engine\exceptions\ConfigurationNotFoundException;
use crystal\core\models\repository\LanguagesRepository;

/**
 * Class TranslationComponent
 * implements the application translation component functionality and helper methods to manage the application language
 *
 * @package     crystal\core
 * @subpackage  crystal\core\components
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class TranslationComponent extends BaseComponent
{
    const COMPONENT_SESSION_NAME = '_crystal-tc';

    const CONFIGURATION_DEFAULT_LANGUAGE = 'crystal.translation.default_language';
    const CONFIGURATION_SOURCE_LANGUAGE = 'crystal.translation.source_language';
    const CONFIGURATION_MULTI_LANGUAGE = 'crystal.translation.multi_language';

    /**
     * @var array will be populated with the application languages
     */
    protected $languages;

    /**
     * @var string will be populated with the current language identity
     */
    protected $currentLanguage;

    /**
     * @var string will be populated with the default language identity
     */
    protected $defaultLanguage;

    /**
     * @var string will be populated with the source language identity
     */
    protected $sourceLanguage;

    /**
     * @var bool will be populated with the multi language configuration value
     */
    protected $multiLanguage;

    /**
     * @var bool will be populated with the component force option value
     */
    private $forceLanguage;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->getConfig(self::CONFIGURATION_DEFAULT_LANGUAGE, 'defaultLanguage');
        $this->getConfig(self::CONFIGURATION_SOURCE_LANGUAGE, 'sourceLanguage');
        $this->getConfig(self::CONFIGURATION_MULTI_LANGUAGE, 'multiLanguage');

        if ( !$this->isLanguagePublished($this->defaultLanguage) ) {
            throw new ConfigurationInvalidException(500,
                \Yii::t('crystal.exceptions',
                    'The default language `{0}` setting value is invalid because the language is not published or is not found!',[
                    $this->defaultLanguage
                ])
            );
        }

        $session = $this->session;

        if( $session !== null ) {
            if ( !$this->validateLanguage($session['current'], $session['force_language']) ) {
                $session = [
                    'current' => $this->defaultLanguage,
                    'source' => $this->sourceLanguage,
                    'multi_language' => $this->multiLanguage,
                    'force_language' => false
                ];
            }
        }


        if ( $session === null ) {
            $session = [
                'current' => $this->defaultLanguage,
                'source' => $this->sourceLanguage,
                'multi_language' => $this->multiLanguage,
                'force_language' => false
            ];
        }

        $this->forceLanguage = $session['force_language'];
        $this->currentLanguage = $session['current'];

        \Yii::$app->language = $session['current'];
        $this->setSession($session);
    }

    /**
     * Set the application current language based on required parameters
     * @param string $identity the language identity
     * @param bool $forceLanguage set this parameter to true if you want to force the language
     * @return bool whatever the language was updated or not
     */
    public function setLanguage( string $identity, bool $forceLanguage = false ) : bool
    {
        if ( $this->currentLanguage === $identity ) {
            return false;
        }

        if ( !$this->validateLanguage($identity, $forceLanguage) ) {
            return false;
        }

        $this->setSession([
            'current' => $identity,
            'source' => $this->sourceLanguage,
            'multi_language' => $this->multiLanguage,
            'force_language' => $forceLanguage
        ]);

        $this->currentLanguage = $identity;
        $this->forceLanguage = $forceLanguage;

        return true;
    }

    /**
     * Validates the language identity if is a valid language for the application
     * @param string $identity the language identity
     * @param bool $forceLanguage if the force language is true then the method will escape the language is_published validation
     * @return bool whatever the language is valid or not
     */
    public function validateLanguage( string $identity, bool $forceLanguage ) : bool
    {
        if ( $identity === $this->defaultLanguage ) {
            return true;
        }

        if ( !array_key_exists($identity, $this->getAllLanguages()) ) {
            return false;
        }

        if ( $forceLanguage === false && $this->isLanguagePublished($identity) === false ) {
            return false;
        }

        if ( $forceLanguage === false && $identity !== $this->defaultLanguage ) {
            return false;
        }

        if ( $this->multiLanguage === false && $identity !== $this->defaultLanguage && $forceLanguage === false ) {
            return false;
        }

        return true;
    }

    /**
     * Validates if the language exists and if exists the language [[is_published]] property is true
     * @param string $identity the language identity
     * @return bool whatever the language is published or not
     */
    public function isLanguagePublished( string $identity ) : bool
    {
        $applicationLanguages = $this->getAllLanguages(true);
        return ( isset($applicationLanguages[$identity]) ) ? true : false;
    }

    /**
     * Get the application current language identity value
     * @return string|null the current language identity value, null if the translation session is not yet initialized
     */
    public function getCurrentLanguage() : ? string
    {
        return $this->currentLanguage;
    }

    /**
     * Get the application default language identity value
     * @return string the default language identity value
     */
    public function getDefaultLanguage() : string
    {
        return $this->defaultLanguage;
    }

    /**
     * Get the application source language identity value
     * @return string the source language identity value
     */
    public function getSourceLanguage() : string
    {
        return $this->sourceLanguage;
    }

    /**
     * Get all application languages
     * @param bool $publishedLanguages set this true if you want filter just the published languages
     * @return array the languages data
     */
    public function getAllLanguages( bool $publishedLanguages = false ) : array
    {
        if ( $this->languages !== null ) {
            $languages = $this->languages;
        }

        if ( $this->languages === null ) {
            $languages = LanguagesRepository::findFormattedLanguages();
        }

        if ( $publishedLanguages === true ) {
            foreach ( $languages as $identity => $isPublished ) :
                if ( $isPublished === false ) {
                    unset($languages[$identity]);
                }
            endforeach;
        }

        return $languages;
    }

    /**
     * {@inheritdoc}
     * not implementing since the component is not using cache storage
     */
    public function generateCacheData(): array{}
}