<?php

namespace crystal\core\components;

use Yii;
use yii\base\Component;
use yii\helpers\HtmlPurifier;
use yii\helpers\Inflector;
use yii\helpers\Markdown;
use yii\helpers\StringHelper;

/**
 * Class ThemeComponent
 * handles the application theme system and implements helper methods to manage the application breadcrumbs and other
 * default theme components
 *
 * @package     crystal\core
 * @subpackage  crystal\core\components
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class ThemeComponent extends Component
{
    /**
     * @var string will be populated with the application theme name
     */
    private $themeName;

    /**
     * @var array will pe populated with the application breadcrumbs
     */
    private $breadcrumbs = [];

    /**
     * Retrieves the application current theme name
     * @return string the theme name
     */
    public function getTheme() : string
    {
        return $this->themeName;
    }

    /**
     * Set the application theme
     * @param string $themeName the theme name
     */
    public function setTheme( string $themeName ) : void
    {
        Yii::$app->view->theme = new \yii\base\Theme([
            'pathMap' => [
                '@app/views' => '@app/web/themes/' . $themeName . '/views/',
                '@app/modules' => '@app/web/themes/' . $themeName . '/views/'
            ],
            'baseUrl' => '@web',
        ]);

        $this->themeName = $themeName;
    }

    /**
     * Retrieves the current action layout name
     * @return string the layout name
     */
    public function getLayout()
    {
        return Yii::$app->layout;
    }

    /**
     * Set the current action layout
     * @param string $layoutName the layout name
     */
    public function setLayout( string $layoutName ) : void
    {
        Yii::$app->layout = $layoutName;
    }

    /**
     * Retrieves the application breadcrumbs
     * @return array the application breadcrumbs
     */
    public function getBreadcrumbs() : array
    {
        return $this->breadcrumbs;
    }

    /**
     * Add a breadcrumb to the breadcrumbs tree structure
     * @param string $name the name of the breadcrumb
     * @param string|null $routeIdentity the route identity for the breadcrumb, if null then the breadcrumb will not have link
     * @param array $routeParameters the parameters for the requested route
     */
    public function addBreadcrumb( string $name, $routeIdentity = null, array $routeParameters = array() ) : void
    {
        if ( $routeIdentity === null ) {
            $routeIdentity = 'no-route';
        }

        $this->breadcrumbs[Inflector::slug($name , '-', true) . '-' . $routeIdentity] = [
            'name' => $name,
            'identity' => $routeIdentity,
            'parameters' => $routeParameters
        ];
    }

    /**
     * Removes a breadrumb from the breadcrumbs tree structure
     * @param string $name the name of the breadcrumb
     * @param string $routeIdentity the route identity for the breadcrumb, if null then the breadcrumb will not have link
     * @return bool whatever the breadcrumb was removed or not
     */
    public function removeBreadcrumb( string $name, $routeIdentity = null) : bool
    {
        if ( $routeIdentity === null ) {
            $routeIdentity = 'no-route';
        }

        if ( isset($this->breadcrumbs[Inflector::slug($name , '-', true) . '-' . $routeIdentity]) ) {
            unset($this->breadcrumbs[Inflector::slug($name , '-', true)  . '-' . $routeIdentity]);
            return true;
        }

        return false;
    }

    /**
     * Cleans the breadcrumbs tree by reseting the value to an empty tree
     */
    public function cleanBreadcrumbs() : void
    {
        $this->breadcrumbs = [];
    }
}