<?php

namespace crystal\core\components;

use Yii;

use crystal\core\engine\BaseComponent;
use crystal\core\engine\exceptions\ConfigurationNotFoundException;
use crystal\core\models\repository\ConfigurationsRepository;

/**
 * Class ConfigurationComponent
 * implements the application configuration functionality and helper methods to retrieve and cache configuration data
 *
 * @package     crystal\core
 * @subpackage  crystal\core\components
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class ConfigurationComponent extends BaseComponent
{
    const COMPONENT_CACHE_NAME = 'configuration';

    /**
     * Retrieves a configuration value based on [[identity]] and [[language_identity]] properties of the configuration
     * @param string $identity the configuration identity
     * @param string|null $languageIdentity the configuration language identity, if null will use the application current language
     * @return mixed the value of the configuration
     * @throws ConfigurationNotFoundException if the configuration is not found
     */
    public function getConfiguration( string $identity, string $languageIdentity = null )
    {
        if ( $languageIdentity === null ) {
            $languageIdentity = Yii::$app->translation->getLanguage();
        }

        if ( Yii::$app->isProduction() ) {
            if ( $settingsCache !== null ) {
                if( isset($settingsCache[$languageIdentity][$identity]) )  {
                    $setting = $settingsCache[$languageIdentity][$identity];
                } else {
                    $setting = null;
                }
            }
        }

        if ( !isset($configuration) ) {
            $configuration = ConfigurationsRepository::findByIdentityAndLanguageFormatted($identity, $languageIdentity);
        }

        if ( $configuration === null ) {
            throw new ConfigurationNotFoundException(500,
                Yii::t('crystal.exceptions', 'The configuration with identity `{0}` for `{1}` language was not found in cache or database storage!', [
                    $identity,
                    $languageIdentity
                ])
            );
        }

        return ($configuration['is_serialized']) ? unserialize($configuration['value']) : $configuration['value'];
    }

    /**
     * Retrieves a global configuration value based on `$identity` parameter
     * @param string $identity the configuration identity
     * @return mixed the value of the configuration
     * @see getConfiguration() the method which handles the configuration retrievement
     * @throws ConfigurationNotFoundException if the configuration is not found
     */
    public function getGlobalConfiguration( string $identity )
    {
        return $this->getConfiguration($identity, 'global');
    }

    /**
     * @return array
     */
    public function generateCacheData(): array
    {

    }
}