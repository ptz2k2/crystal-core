<?php

defined('CRYSTAL_ENV_DEV') or define('CRYSTAL_ENV_DEV', 'development');
defined('CRYSTAL_ENV_PROD') or define('CRYSTAL_ENV_PROD', 'production');

/**
 * Class Crystal
 * the main class file that retrieves or generates the application configuration based on environment value
 *
 * The class verifies if there is a cached configuration if the environment is production and if exists will return that
 * else generates the configuration from the application configuration and environment configuration
 *
 * The flow of the configuration generation method is to retrieve the `application configuration` and the `environment
 * configuration` based on environment and merge the environment configuration into the application configuration
 * by overriding the application configuration if the environment configuration has the same array key structure
 * @see _mergeArrays() to understand better the merge functionality between the to configurations
 *
 * @property $applicationBasePath the application base path, the value will be populated on class construct
 * @property $crystalBasePath the crystal core base path, the value will be populated on class construct
 * @property $environment the environment of the application, the value can be `development` or `production`
 * @property $generatedConfiguration the generated configuration data for the application, the value will be populated
 * on the class construct
 *
 * @package     crystal\core
 * @subpackage  crystal\core\engine
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class Crystal
{
    const CONFIGURATION_APP_NAME = 'application';
    const CONFIGURATION_ENV_NAME = 'env-';
    const CONFIGURATION_CACHE_NAME = 'configuration';

    /**
     * @var string the application base path, will be populated on class construct
     */
    private $applicationBasePath;

    /**
     * @var string the crystal core base bath, will be populated on class construct
     */
    private $crystalBasePath;

    /**
     * @var string the current application environment value, will be populated on class construct method
     */
    private $environment;

    /**
     * @var array the generated configuration data value, will be populated in getConfiguration method
     */
    private $generatedConfiguration = array();

    /**
     * Populates the class required properties to retrieve or generate the configuration data
     * The method retrieves the configuration data from cache if exists or generates a new one based on envrionment parameter
     * @param string $environment the environment configuration value of the application
     * @throws Exception if the application environment is not valid
     * @uses _generateConfigurationData() the method that retrieves or generates the configuration data
     */
    public function __construct( string $environment )
    {
        if( $environment !== CRYSTAL_ENV_DEV && $environment !== CRYSTAL_ENV_PROD ) {
            throw new Exception('The ' . $environment  . ' value is not a valid application environment', 500);
        }

        $this->environment = $environment;
        $this->applicationBasePath = dirname(__DIR__ ,3) . DIRECTORY_SEPARATOR;
        $this->crystalBasePath = __DIR__ . DIRECTORY_SEPARATOR;

        if( $environment === CRYSTAL_ENV_DEV ) {
            error_reporting(E_ALL);
            ini_set('display_errors', '1');
            defined('YII_DEBUG') or define('YII_DEBUG', true);
            defined('YII_ENV') or define('YII_ENV', 'dev');
        }

        $this->generatedConfiguration = $this->_generateConfigurationData();
    }

    /**
     * Get the application generated configuration
     * @return array the generated configuration value
     */
    public function getConfiguration() : array
    {
        return $this->generatedConfiguration;
    }

    /**
     * Sets the application base with the yii request class
     * @param \yii\web\Request $request the request instance
     */
    public function setApplicationBaseUrl( $request ) : void
    {
        $baseUrl =  str_replace('/web', '', $request->getBaseUrl());
        $this->generatedConfiguration['components']['request']['baseUrl'] = $baseUrl;
    }

    /**
     * Generates the configuration data if the configuration cache file not exists
     * @return array the generated configuration data
     */
    private function _generateConfigurationData() : array
    {
        $cacheConfiguration = $this->_getConfigurationFromFile(self::CONFIGURATION_CACHE_NAME, true);

        if( $cacheConfiguration !== null && $this->environment === CRYSTAL_ENV_PROD ) {
            return $cacheConfiguration;
        }

        $applicationConfiguration = $this->_getConfigurationFromFile(self::CONFIGURATION_APP_NAME);
        $environmentConfiguration = $this->_getConfigurationFromFile(self::CONFIGURATION_ENV_NAME . $this->environment);

        return $this->_mergeArrays($applicationConfiguration, $environmentConfiguration);
    }

    /**
     * Retrieves the configuration file content based on $fileName and $isCache parameters
     * @param string $fileName the file name of the configuration
     * @param bool $isCache set this true if you searching for a cache file
     * @return array|null the configuration file content, null if the configuration file is not founde
     * @throws Exception if the configuration file is not found and it's not a cache file
     */
    private function _getConfigurationFromFile( string $fileName, $isCache = false ) : ? array
    {
        $fileName = $fileName . '.php';
        $filePath = $this->applicationBasePath . 'app' . DIRECTORY_SEPARATOR;

        if( $isCache ) {
            $filePath = $filePath . 'cache' . DIRECTORY_SEPARATOR;
        }

        if( file_exists($filePath . $fileName) ) {
            return include($filePath . $fileName);
        }

        if( !$isCache ) {
            throw new Exception('The configuration file was not found in ' . $filePath . $fileName);
        }

        return null;
    }

    /**
     * Merges two or more arrays into one recursively.
     * If each array has an element with the same string key value, the latter
     * will overwrite the former (different from array_merge_recursive).
     * Recursive merging will be conducted if both arrays have an element of array
     * type and are having the same key.
     * For integer-keyed elements, the elements from the latter array will
     * be appended to the former array.
     * You can use [[UnsetArrayValue]] object to unset value from previous array or
     * [[ReplaceArrayValue]] to force replace former value instead of recursive merging.
     * @param array $a array to be merged to
     * @param array $b array to be merged from. You can specify additional
     * arrays via third argument, fourth argument etc.
     * @return array the merged array (the original arrays are not changed.)
     */
    public function _mergeArrays( array $a, array $b) : array
    {
        $args = func_get_args();
        $res = array_shift($args);

        while ( !empty($args) ) {
            foreach ( array_shift($args) as $k => $v ) :
                if ($v instanceof UnsetArrayValue) {
                    unset($res[$k]);
                } elseif ($v instanceof ReplaceArrayValue) {
                    $res[$k] = $v->value;
                } elseif ( is_int($k) ) {
                    if ( array_key_exists($k, $res) ) {
                        $res[] = $v;
                    } else {
                        $res[$k] = $v;
                    }
                } elseif (is_array($v) && isset($res[$k]) && is_array($res[$k])) {
                    $res[$k] = self::_mergeArrays($res[$k], $v);
                } else {
                    $res[$k] = $v;
                }
            endforeach;
        }

        return $res;
    }
}