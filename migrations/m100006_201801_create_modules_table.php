<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\Modules;

/**
 * Class m100006_201801_create_modules_table
 * on safeUp method the migration creates the `modules` table
 * on safeDown method the migration deletes all the records from `modules` table and drops the table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m100006_201801_create_modules_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Modules::tableName(), [
            'id' => $this->primaryKey(),
            'identity' => $this->string(255)->notNull(),
            'base_path' => $this->string(255)->notNull(),
            'is_core' => $this->boolean()->notNull(),
            'is_installed' => $this->boolean()->notNull(),
            'is_enabled' => $this->boolean()->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ],self::ENGINE_SET);

        $this->createIndex('modules-identity', Modules::tableName(), 'identity', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Modules::tableName(),[]);
        $this->dropIndex('modules-identity', Modules::tableName());
        $this->dropTable(Modules::tableName());
    }
}
