<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\Routes;

/**
 * Class m100001_201801_create_routes_table
 * on safeUp method the migration creates the `routes` table
 * on safeDown method the migration deletes all the records from `routes` table and drops the table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m100001_201801_create_routes_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Routes::tableName(), [
            'unique_id' => $this->string(64)->notNull(),
            'identity' => $this->string(255)->notNull(),
            'language_identity' => $this->string(50)->notNull(),
            'url_rule' => $this->string(255)->notNull(),
            'url_controller' => $this->string(255)->notNull(),
            'url_path' => $this->string(255)->notNull(),
            'http_method' => $this->string(255),
            'is_core' => $this->boolean()->notNull(),
            'sort_order' => $this->integer(11)->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ],self::ENGINE_SET);

        $this->addPrimaryKey('routes-unique-id', Routes::tableName(), ['unique_id']);
        $this->createIndex('routes-identity',Routes::tableName(),'identity');
        $this->createIndex('routes-language-identity',Routes::tableName(),'language_identity');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Routes::tableName(),[]);
        $this->dropIndex('routes-identity',Routes::tableName());
        $this->dropPrimaryKey('routes-language-identity', Routes::tableName());
        $this->dropPrimaryKey('routes-identity', Routes::tableName());
        $this->dropPrimaryKey('routes-unique-id', Routes::tableName());
        $this->dropTable(Routes::tableName());
    }
}
