<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\Rules;

/**
 * Class m100005_201801_create_rules_table
 * on safeUp method the migration creates the `rules` table
 * on safeDown method the migration deletes all the records from `rules` table and drops the table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m100005_201801_create_rules_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Rules::tableName(), [
            'id' => $this->primaryKey(),
            'route_identity' => $this->string('255')->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->string(255)->notNull(),
            'is_core' => $this->boolean()->notNull(),
            'is_published' => $this->boolean()->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ],self::ENGINE_SET);

        $this->createIndex('rules-route-identity', Rules::tableName(), 'route_identity', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Rules::tableName(),[]);
        $this->dropIndex('rules-route-identity', Rules::tableName(), 'rules-route-identity');
        $this->dropTable(Rules::tableName());
    }
}
