<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\Permissions;

/**
 * Class m100010_201801_create_permissions_table
 * on safeUp method the migration creates the `permissions` table
 * on safeDown method the migration deletes all the records from `permissions` table and drops the table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m100010_201801_create_permissions_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Permissions::tableName(), [
            'id' => $this->primaryKey(),
            'role_id' => $this->integer(255)->notNull(),
            'rule_id' => $this->integer(11)->notNull(),
            'has_permission' => $this->boolean()->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ],self::ENGINE_SET);

        $this->createIndex('permissions-role-id', Permissions::tableName(), 'role_id');
        $this->createIndex('permissions-rule-id', Permissions::tableName(), 'rule_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Permissions::tableName(),[]);
        $this->dropIndex('permissions-role-id', Permissions::tableName());
        $this->dropIndex('permissions-rule-id', Permissions::tableName());
        $this->dropTable(Permissions::tableName());
    }
}
