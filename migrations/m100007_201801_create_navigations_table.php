<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\Navigations;

/**
 * Class m100007_201801_create_navigations_table
 * on safeUp method the migration creates the `navigations` table
 * on safeDown method the migration deletes all the records from `navigations` table and drops the table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m100007_201801_create_navigations_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Navigations::tableName(),[
            'id' => $this->primaryKey(),
            'identity' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->string(255)->notNull(),
            'html_options' => $this->text(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ],self::ENGINE_SET);

        $this->createIndex('navigations-identity', Navigations::tableName(), 'identity');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Navigations::tableName(),[]);
        $this->dropIndex('navigations-identity', Navigations::tableName());
        $this->dropTable(Navigations::tableName());
    }
}
