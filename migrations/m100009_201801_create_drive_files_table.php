<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\DriveFiles;

/**
 * Class m100009_201801_create_drive_files_table
 * on safeUp method the migration creates the `drive_files` table
 * on safeDown method the migration deletes all the records from `drive_files` table and drops the table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m100009_201801_create_drive_files_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(DriveFiles::tableName(), [
            'unique_id' => $this->string(64)->notNull(),
            'entity_type' => $this->string(255)->notNull(),
            'identity' => $this->string(255)->notNull(),
            'file_name' => $this->string(255)->notNull(),
            'file_type' => $this->string(100)->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ],self::ENGINE_SET);

        $this->addPrimaryKey('drive-files-unique-id', DriveFiles::tableName(), ['unique_id']);
        $this->createIndex('drive-files-identity', DriveFiles::tableName(), 'identity');
        $this->createIndex('drive-files-entity-type', DriveFiles::tableName(), 'entity_type');
        $this->createIndex('drive-files-file-name', DriveFiles::tableName(), 'file_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(DriveFiles::tableName(),[]);
        $this->dropIndex('drive-files-file-name', DriveFiles::tableName());
        $this->dropIndex('drive-files-entity-type', DriveFiles::tableName());
        $this->dropIndex('drive-files-identity', DriveFiles::tableName());
        $this->dropPrimaryKey('drive-files-unique-id', DriveFiles::tableName());
        $this->dropTable(DriveFiles::tableName());
    }
}
