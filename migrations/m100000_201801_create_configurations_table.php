<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\Configurations;

/**
 * Class m100000_201801_create_configurations_table
 * on safeUp method the migration creates the `configurations` table
 * on safeDown method the migration deletes all the records from `configurations` table and drops the table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m100000_201801_create_configurations_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Configurations::tableName(), [
            'id' => $this->primaryKey(),
            'identity' => $this->string(255)->notNull(),
            'language_identity' => $this->string(50)->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->string(255),
            'value' => $this->text(),
            'is_serialized' => $this->boolean()->notNull(),
            'is_core' => $this->boolean()->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ],self::ENGINE_SET);

        $this->createIndex('configurations-identity',Configurations::tableName(),'identity');
        $this->createIndex('configurations-language-identity',Configurations::tableName(),'language_identity');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Configurations::tableName(),[]);
        $this->dropTable('configurations-language-identity',Configurations::tableName());
        $this->dropIndex('configurations-identity',Configurations::tableName());
        $this->dropTable(Configurations::tableName());
    }
}
