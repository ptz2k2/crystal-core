<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\NavigationItems;

/**
 * Class m100008_201801_create_navigation_items_table
 * on safeUp method the migration creates the `navigation_items` table
 * on safeDown method the migration deletes all the records from `navigation_items` table and drops the table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m100008_201801_create_navigation_items_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(NavigationItems::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'navigation_identity' => $this->string(255)->notNull(),
            'language_identity' => $this->string(50)->notNull(),
            'route_identity' => $this->string(255)->notNull(),
            'route_parameters' => $this->text(),
            'parent_id' => $this->integer(11),
            'source_item_id' => $this->integer(11),
            'sort_order' => $this->integer(11),
            'html_options' => $this->text(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ],self::ENGINE_SET);

        $this->createIndex('navigations-items-navigation-identity', NavigationItems::tableName(), 'navigation_identity');
        $this->createIndex('navigations-items-language-identity', NavigationItems::tableName(), 'language_identity');
        $this->createIndex('navigations-items-route-identity', NavigationItems::tableName(), 'route_identity');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(NavigationItems::tableName(),[]);
        $this->dropIndex('navigations-items-route-identity', NavigationItems::tableName());
        $this->dropIndex('navigations-items-language-identity', NavigationItems::tableName());
        $this->dropIndex('navigations-items-navigation-identity', NavigationItems::tableName());
        $this->dropTable(NavigationItems::tableName());
    }
}
