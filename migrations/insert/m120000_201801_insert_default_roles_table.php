<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\Roles;
use crystal\core\helpers\DataGeneratorHelper;

/**
 * Class m120000_201801_insert_default_roles_table
 * on safeUp method the migration inserts the default roles into the `roles` table
 * on safeDown method the migration deletes all the records that where inserted in safeUp method from `roles` table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations\insert
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m120000_201801_insert_default_roles_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(Roles::tableName(), array (
            'identity' => 'guest',
            'name' => 'roles.guest.name',
            'description' => 'roles.guest.description',
            'is_core' => (int) true,
            'is_published' => (int) true,
            'created_at' => DataGeneratorHelper::currentDatetime()
        ));

        $this->insert(Roles::tableName(), array (
            'identity' => 'member',
            'name' => 'roles.member.name',
            'description' => 'roles.member.description',
            'is_core' => (int) true,
            'is_published' => (int) true,
            'created_at' => DataGeneratorHelper::currentDatetime()
        ));

        $this->insert(Roles::tableName(), array (
            'identity' => 'administrator',
            'name' => 'roles.administrator.name',
            'description' => 'roles.administrator.description',
            'is_core' => (int) true,
            'is_published' => (int) true,
            'created_at' => DataGeneratorHelper::currentDatetime()
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Roles::tableName(),['identity' => 'administrator']);
        $this->delete(Roles::tableName(),['identity' => 'member']);
        $this->delete(Roles::tableName(),['identity' => 'guest']);
    }
}
