<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\Routes;
use crystal\core\helpers\DataGeneratorHelper;

/**
 * Class m120001_201801_insert_maintenance_routes_table
 * on safeUp method the migration inserts the maintenance component routes into the `routes` table
 * on safeDown method the migration deletes all the records that where inserted in safeUp method from `routes` table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations\insert
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m120001_201801_insert_maintenance_routes_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(Routes::tableName(), [
            'unique_id' => DataGeneratorHelper::generateUniqueId(Routes::class),
            'identity' => 'crystal-maintenance-index',
            'language_identity' => 'en-US',
            'url_rule' => 'maintenance',
            'url_controller' => 'maintenance/index',
            'url_path' => 'maintenance',
            'is_core' => (int) true,
            'sort_order' => 1,
            'created_at' => DataGeneratorHelper::currentDatetime()
        ]);

        $this->insert(Routes::tableName(), [
            'unique_id' => DataGeneratorHelper::generateUniqueId(Routes::class),
            'identity' => 'crystal-maintenance-enable-key',
            'language_identity' => 'en-US',
            'url_rule' => 'maintenance/enable/<key:\w+>',
            'url_controller' => 'maintenance/enable-key',
            'url_path' => 'maintenance/enable/{key}',
            'is_core' => (int) true,
            'sort_order' => 2,
            'created_at' => DataGeneratorHelper::currentDatetime()
        ]);

        $this->insert(Routes::tableName(), [
            'unique_id' => DataGeneratorHelper::generateUniqueId(Routes::class),
            'identity' => 'crystal-maintenance-disable-key',
            'language_identity' => 'en-US',
            'url_rule' => 'maintenance/disable',
            'url_controller' => 'maintenance/disable-key',
            'url_path' => 'maintenance/disable',
            'is_core' => (int) true,
            'sort_order' => 3,
            'created_at' => DataGeneratorHelper::currentDatetime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Roles::tableName(),['identity' => 'crystal-maintenance-disable-key']);
        $this->delete(Roles::tableName(),['identity' => 'crystal-maintenance-enable-key']);
        $this->delete(Roles::tableName(),['identity' => 'crystal-maintenance-index']);
    }
}
