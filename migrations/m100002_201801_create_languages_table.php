<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\Languages;

/**
 * Class m100002_201801_create_languages_table
 * on safeUp method the migration creates the `languages` table
 * on safeDown method the migration deletes all the records from `languages` table and drops the table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m100002_201801_create_languages_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Languages::tableName(), [
            'id' => $this->primaryKey(),
            'identity' => $this->string(50)->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->string(255)->notNull(),
            'is_core' => $this->boolean()->notNull(),
            'is_published' => $this->boolean()->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ],self::ENGINE_SET);

        $this->createIndex('languages-identity',Languages::tableName(),'identity' ,true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Languages::tableName(),[]);
        $this->dropIndex('languages-identity',Languages::tableName());
        $this->dropTable(Languages::tableName());
    }
}
