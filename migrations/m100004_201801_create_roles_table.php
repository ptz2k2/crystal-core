<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\Roles;

/**
 * Class m100004_201801_create_roles_table
 * on safeUp method the migration creates the `roles` table
 * on safeDown method the migration deletes all the records from `roles` table and drops the table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m100004_201801_create_roles_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Roles::tableName(), [
            'id' => $this->primaryKey(),
            'identity' => $this->string('255')->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->string(255)->notNull(),
            'is_core' => $this->boolean()->notNull(),
            'is_published' => $this->boolean()->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ],self::ENGINE_SET);

        $this->createIndex('roles-identity',Roles::tableName(),'identity', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Roles::tableName(),[]);
        $this->dropIndex('roles-identity',Roles::tableName());
        $this->dropTable(Roles::tableName());
    }
}
