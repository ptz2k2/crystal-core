<?php

use crystal\core\engine\BaseMigration;
use crystal\core\models\entity\Identity;

/**
 * Class m100003_201801_create_identity_table
 * on safeUp method the migration creates the `identity` table
 * on safeDown method the migration deletes all the records from `identity` table and drops the table
 *
 * @package     crystal\core
 * @subpackage  crystal\core\migrations
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class m100003_201801_create_identity_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Identity::tableName(), [
            'unique_id' => $this->string(64)->notNull(),
            'email_address' => $this->string(255)->notNull(),
            'password' => $this->string(64)->notNull(),
            'first_name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'role_id' => $this->integer(11)->notNull(),
            'is_validated' => $this->boolean()->notNull(),
            'is_newsletter' => $this->boolean()->notNull(),
            'authorization_key' => $this->string(64),
            'access_token' => $this->string(64),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ],self::ENGINE_SET);

        $this->addPrimaryKey('identity-unique-id', Identity::tableName(), ['unique_id']);
        $this->createIndex('identity-email-address',Identity::tableName(),'email_address', true);
        $this->createIndex('identity-authorization-key', Identity::tableName(), 'authorization_key');
        $this->createIndex('identity-access-token', Identity::tableName(), 'access_token');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(Identity::tableName(),[]);
        $this->dropIndex('identity-access-token',Identity::tableName());
        $this->dropIndex('identity-authorization-key',Identity::tableName());
        $this->dropIndex('identity-email-address',Identity::tableName());
        $this->dropPrimaryKey('identity-unique-id', Identity::tableName());
        $this->dropTable(Identity::tableName());
    }
}
