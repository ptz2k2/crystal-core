<?php

namespace crystal\core\models\form;

use Yii;
use yii\base\Model;
use crystal\core\models\entity\Identity;

/**
 * Class LoginForm
 * implements the login form fields and validation rules
 *
 * @package     crystalEngine
 * @subpackage  crystalEngine\models\form
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class LoginForm extends Model
{
    /**
     * @var string $email_address the login form email address field value
     */
    public $email_address;

    /**
     * @var string $password the login form password field value
     */
    public $password;

    /**
     * @var Identity|Null will be poppulated with the identity
     */
    private $_identity;

    /**
     * {@inheritdoc}
     */
    public function rules() : array
    {
        return [
            [['email_address', 'password'], 'required'],
            ['email_address', 'email'],
            ['password', 'validatePassword']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() : array
    {
        return [
            'email_address' => Yii::t('crystal.forms', 'Email address'),
            'password' => Yii::t('crystal.forms', 'Password')
        ];
    }

    /**
     * Validates the [[password]] if it is correct for the requested Identity
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     * @return void
     */
    public function validatePassword( $attribute, $params ) : void
    {
        if ( !$this->hasErrors() ) {
            $identity = $this->getIdentity();

            if ( !$identity || !$identity->validatePassword($this->password) ) {
                $this->addError($attribute, Yii::t('crystal.forms', 'Incorrect email address or password'));
            }
        }
    }

    /**
     * Logs in the Identity based on provided [[email_address]] and [[password]] properties
     * @return bool whatevet the user was logged in or not
     */
    public function login() : bool
    {
        if ( $this->validate() ) {
            return Yii::$app->user->login($this->getIdentity(),0);
        }

        return false;
    }

    /**
     * Retrieves the identity based on [[email_address]] property
     * @return Identity|null identity if the ActiveRecord found results else null
     */
    public function getIdentity() : ?Identity
    {
        if ( $this->_identity === null ) {
            $this->_identity = Identity::findByEmail( $this->email_address);
        }

        return $this->_identity;
    }
}
