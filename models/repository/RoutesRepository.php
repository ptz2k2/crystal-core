<?php

namespace crystal\core\models\repository;

use crystal\core\engine\interfaces\RepositoryCacheInterface;
use crystal\core\models\entity\Routes;

/**
 * Class RoutesRepository
 * implements helper methods for the active record class to generate or search in database based on multiple criterias
 * implements a cache interface where the repository stores the application configuration generated routes data
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\repository
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class RoutesRepository extends Routes implements RepositoryCacheInterface
{
    const CACHE_NAME = 'repository-routes';

    /**
     * Finds all routes sorted by sort order and formatted for the application url rules configuration
     * Note that this method will return the data retrieved from cache storage if the environment is production and
     * the cache file exists
     * @return array the formatted active record result data
     */
    public static function findAllFormatted() : array
    {
        $cachedData = self::getCache();

        if ( \Yii::$app->isProduction() && $cachedData !== null ) {
            return $cachedData;
        }

        $formattedRoutes = self::generateCacheData();

        self::setCache($formattedRoutes);
        return $formattedRoutes;
    }

    /**
     * {@inheritdoc}
     */
    public static function getCache() : ? array
    {
        $cacheData = \Yii::$app->cache->get(static::CACHE_NAME);
        return ( $cacheData === false ) ? null : $cacheData;
    }

    /**
     * {@inheritdoc}
     */
    public static function setCache( array $value ) : void
    {
        \Yii::$app->cache->set(static::CACHE_NAME, $value);
    }

    /**
     * {@inheritdoc}
     */
    public static function generateCacheData() : array
    {
        $routes = self::find()->orderBy(['sort_order' => SORT_ASC])->all();

        if ( empty($routes) ) {
            return array();
        }

        $formattedRoutes = [];

        foreach( $routes as $route ) :

            $formattedRoutes[] = array(
                'pattern' => $route->url_rule,
                'route' => $route->url_controller
            );

        endforeach;

        return $formattedRoutes;
    }
}