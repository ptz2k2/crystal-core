<?php

namespace crystal\core\models\repository;

use crystal\core\models\entity\NavigationItems;

/**
 * Class NavigationItemsRepository
 * implements helper methods for the active record class to generate or search in database based on multiple criterias
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\repository
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class NavigationItemsRepository extends NavigationItems
{
    
}