<?php

namespace crystal\core\models\repository;

use crystal\core\models\entity\Navigations;

/**
 * Class NavigationsRepository
 * implements helper methods for the active record class to generate or search in database based on multiple criterias
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\repository
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class NavigationsRepository extends Navigations
{
    /**
     * Retrieves a navigation and his items from database based on navigation identity property
     * generates a formatted data with the retrieved navigation and navigation items
     * @param string $identity the navigation identity
     * @return array|null array if the navigation was found else will return null
     */
    public static function findNavigationFormatted( string $identity ) : ? array
    {
        $navigation = self::findByIdentity($identity);

        var_dump($navigation);
    }
}