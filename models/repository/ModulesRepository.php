<?php

namespace crystal\core\models\repository;

use crystal\core\models\entity\Modules;

/**
 * Class ModulesRepository
 * implements helper methods for the active record class to generate or search in database based on multiple criterias
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\repository
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class ModulesRepository extends Modules
{
    /**
     * Find all modules where the [[is_enabled]] property is true
     * @return array the result of active record query
     */
    public static function findAllEnabled() : array
    {
        return self::findAllByAttributes([
            'is_enabled' => (int) true
        ]);
    }

    /**
     * Find all modules where the [[is_installed]] property is true
     * @return array the result of active record query
     */
    public static function findAllInstalled()
    {
        return self::findAllByAttributes([
            'is_installed' => (int) true
        ]);
    }
}