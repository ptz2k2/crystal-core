<?php

namespace crystal\core\models\repository;

use crystal\core\models\entity\Configurations;

/**
 * Class ConfigurationsRepository
 * implements helper methods for the active record class to generate or search in database based on multiple criterias
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\repository
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class ConfigurationsRepository extends Configurations
{
    /**
     * Retrieves a configuration based on [[identity]] and [[language_identity]] criteria formatted
     *
     * ```
     * array (
     *  'is_serialized' => Configuration->is_serialized
     *  'value' => Configuration->value
     * )
     * ```
     *
     * @param $identity the language identity value
     * @param $languageIdentity string the language identity value
     * @return array|null the formatted configuration data value, null if the configuration is not found
     */
    public static function findByIdentityAndLanguageFormatted( string $identity, string $languageIdentity)
    {
        $configuration = parent::findByIdentityAndLanguage($identity, $languageIdentity);

        if ( !$configuration ) {
            return null;
        }

        return array (
            'is_serialized' => $configuration->is_serialized,
            'value' => $configuration->value,
        );
    }
}