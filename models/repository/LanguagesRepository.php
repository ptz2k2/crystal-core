<?php

namespace crystal\core\models\repository;

use crystal\core\models\entity\Languages;

/**
 * Class LanguagesRepository
 * implements helper methods for the active record class to manage and search in database based on multiple criteria
 * implements helper methods to format the entity data
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\repository
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class LanguagesRepository extends Languages
{
    /**
     * Retrieves the application languages and formats the value
     *
     * ```
     * array (
     *   [[language_identity]] => [[is_published]]
     * )
     * ```
     * @return array the formatted languages value
     */
    public static function findFormattedLanguages() : array
    {
        $languages = self::findAllByAttributes([]);

        if ( empty($languages) ) {
            return [];
        }

        $formattedLanguages = [];

        foreach ( $languages as $language ) :
            $formattedLanguages[$language->identity] = (bool) $language->is_published;
        endforeach;

        return $formattedLanguages;
    }
}