<?php

namespace crystal\core\models\repository;

use Yii;
use crystal\core\models\entity\Identity;

class IdentityRepository extends Identity
{
    /**
     * Generates the identity properties
     * @param IdentityRepository $identity the poppulated identity data
     * @return IdentityRepository the identity with the generated core data
     */
    public static function generateIdentity( IdentityRepository $identity ) : IdentityRepository
    {
        $identity->role_id = 0;
        $identity->is_validated = 1;
        $identity->is_newsletter = 1;
        $identity->password = Yii::$app->security->generatePasswordHash($identity->password);

        return $identity;
    }
}