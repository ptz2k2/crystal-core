<?php

namespace crystal\core\models\entity;

use yii\web\IdentityInterface;
use crystal\core\engine\BaseActiveRecord;

/**
 * Class Identity
 * implements the `identity` entity model for active record
 *
 * @property string $unique_id
 * @property string $email_address
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property int $role_id
 * @property int $is_validated
 * @property int $is_newsletter
 * @property string authorization_key
 * @property string $access_token
 * @property string $created_at
 * @property string $updated_at
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\entity
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class Identity extends BaseActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%identity}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email_address', 'password', 'first_name', 'last_name', 'role_id', 'is_validated', 'is_newsletter'], 'required'],
            [['role_id', 'is_validated', 'is_newsletter'], 'integer'],
            [['authorization_key', 'access_token'], 'string', 'min' => 64, 'max' => 64],
            [['email_address', 'password', 'first_name', 'last_name'], 'string', 'max' => 255],
            [['created_at','updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'unique_id' => 'Unique ID',
            'email_address' => 'Email Address',
            'password' => 'Password',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'role_id' => 'Role ID',
            'is_validated' => 'Is Validated',
            'is_newsletter' => 'Is Newsletter',
            'authorization_key' => 'Authorization Key',
            'access_token' => 'Access Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Returns the current identity [[unique_id]] property
     * @return string the unique_id property value
     */
    public function getId() : string
    {
        return $this->unique_id;
    }

    /**
     * Returns the current identity authentication key
     */
    public function getAuthKey()
    {
        return $this->authorization_key;
    }

    /**
     * Validated the [[$authKey]] if is valid for the requested identity
     * @return bool whatever the authKey is valid or not
     */
    public function validateAuthKey( $authKey )
    {
        if ( $authKey === $this->authorization_key ) {
            return true;
        }

        return false;
    }

    /**
     * Find identity based on [[unique_id]] property
     * @param string $uniqueId the unique_id of the identity
     * @return Identity|null identity if the ActiveRecord found result else null
     */
    public static function findIdentity( $uniqueId )
    {
        return static::findOne(['unique_id' => $uniqueId]);
    }

    /**
     * Find identity based on [[email_address]] property
     * @param string $emailAddress the email_address of the identity
     * @return Identity|null identity if the ActiveRecord found result else null
     */
    public static function findByEmail( $emailAddress )
    {
        return static::findOne(['email_address' => $emailAddress]);
    }

    /**
     * Find identity based on [[access_token]] property
     * @param string $token the access_token of the identity
     * @param string $type the type of the access token
     * @return Identity|null identity if the ActiveRecord found result else null
     */
    public static function findIdentityByAccessToken( $token, $type = null )
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Validates if the requested password is same as the current Identity password
     * @param string $inputPassword the input password
     * @return boolean whatever the password is valid or not
     */
    public function validatePassword( string $inputPassword ) : bool
    {
        if ( \Yii::$app->security->validatePassword($inputPassword, $this->password) ) {
            return true;
        }

        return false;
    }
}
