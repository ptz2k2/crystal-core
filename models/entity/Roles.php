<?php

namespace crystal\core\models\entity;

use crystal\core\engine\BaseActiveRecord;

/**
 * Class Roles
 * implements the `roles` entity model for active record
 *
 * @property int $id
 * @property string $identity
 * @property string $name
 * @property string $description
 * @property int $is_core
 * @property int $is_published
 * @property string $created_at
 * @property string $updated_at
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\entity
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class Roles extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%roles}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identity', 'name', 'description', 'is_core', 'is_published'], 'required'],
            [['is_core', 'is_published'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['identity', 'name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identity' => 'Identity',
            'name' => 'Name',
            'description' => 'Description',
            'is_active' => 'Is Active',
            'is_core' => 'Is Core',
            'created_at' => 'Created At',
        ];
    }
}
