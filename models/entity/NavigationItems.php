<?php

namespace crystal\core\models\entity;

use crystal\core\engine\BaseActiveRecord;

/**
 * Class NavigationItems
 * implements the `navigation_items` entity model for active record
 *
 * @property int $id
 * @property string $name
 * @property string $navigation_identity
 * @property string $language_identity
 * @property string $route_identity
 * @property string $route_parameters
 * @property int $parent_id
 * @property int $source_item_id
 * @property int $sort_order
 * @property string $html_options
 * @property string $created_at
 * @property string $updated_at
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\entity
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class NavigationItems extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%navigation_items}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'navigation_identity', 'language_identity', 'route_identity'], 'required'],
            [['route_parameters', 'html_options'], 'string'],
            [['parent_id', 'source_item_id', 'sort_order'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'navigation_identity', 'route_identity'], 'string', 'max' => 255],
            [['language_identity'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'navigation_identity' => 'Navigation Identity',
            'language_identity' => 'Language Identity',
            'route_identity' => 'Route Identity',
            'route_parameters' => 'Route Parameters',
            'parent_id' => 'Parent ID',
            'source_item_id' => 'Source Item ID',
            'sort_order' => 'Sort Order',
            'html_options' => 'Html Options',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
