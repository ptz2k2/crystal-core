<?php

namespace crystal\core\models\entity;

use crystal\core\engine\BaseActiveRecord;

/**
 * Class Modules
 * implements the `modules` entity model for active record
 *
 * @property int $id
 * @property string $identity
 * @property string $namespace
 * @property int $is_core
 * @property int $is_installed
 * @property int $is_enabled
 * @property string $created_at
 * @property string $updated_at
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\entity
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class Modules extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%modules}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identity', 'namespace', 'is_core', 'is_installed', 'is_enabled'], 'required'],
            [['is_core', 'is_installed', 'is_enabled'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['identity', 'namespace'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identity' => 'Identity',
            'namespace' => 'Namespace',
            'is_core' => 'Is Core',
            'is_installed' => 'Is Installed',
            'is_enabled' => 'Is Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
