<?php

namespace crystal\core\models\entity;

use crystal\core\engine\BaseActiveRecord;

/**
 * Class Configurations
 * implements the `configurations` entity model for active record
 *
 * @property int $id
 * @property string $identity
 * @property string $language_identity
 * @property string $name
 * @property string $description
 * @property string $value
 * @property int $is_serialized
 * @property int $is_core
 * @property string $created_at
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\entity
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class Configurations extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%configurations}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identity', 'name', 'is_serialized', 'is_core'], 'required'],
            [['value'], 'string'],
            [['is_serialized', 'is_core'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['identity', 'description'], 'string', 'max' => 255],
            [['language_identity', 'name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identity' => 'Identity',
            'language_identity' => 'Language Identity',
            'name' => 'Name',
            'description' => 'Description',
            'value' => 'Value',
            'is_serialized' => 'Is Serialized',
            'is_core' => 'Is Core',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
