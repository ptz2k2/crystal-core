<?php

namespace crystal\core\models\entity;

use crystal\core\engine\BaseActiveRecord;

/**
 * Class Navigations
 * implements the `navigations` entity model for active record
 *
 * @property int $id
 * @property string $identity
 * @property string $name
 * @property string $description
 * @property string $html_options
 * @property string $created_at
 * @property string $updated_at
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\entity
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class Navigations extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%navigations}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identity', 'name', 'description'], 'required'],
            [['html_options'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['identity', 'name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identity' => 'Identity',
            'name' => 'Name',
            'description' => 'Description',
            'html_options' => 'Html Options',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
