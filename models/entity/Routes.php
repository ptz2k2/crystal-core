<?php

namespace crystal\core\models\entity;

use crystal\core\engine\BaseActiveRecord;

/**
 * Class Routes
 * implements the `routes` entity model for active record
 *
 * @property int $id
 * @property string $identity
 * @property string $language_identity
 * @property string $url_rule
 * @property string $url_controller
 * @property string $url_path
 * @property string $http_method
 * @property int $is_core
 * @property int $sort_order
 * @property string $created_at
 * @property string $updated_at
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\entity
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class Routes extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%routes}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identity', 'language_identity', 'url_rule', 'url_controller', 'url_path', 'is_core', 'sort_order'], 'required'],
            [['is_core'], 'integer'],
            [['created_at','updated_at'], 'safe'],
            [['sort_order'], 'integer', 'max' => '11'],
            [['language_identity'], 'string', 'max' => 50],
            [['unique_id'], 'string', 'min' => 64, 'max' => 64],
            [['identity','url_rule', 'url_controller', 'url_path', 'http_method'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identity' => 'Identity',
            'language_identity' => 'Language Identity',
            'url_rule' => 'Url Rule',
            'url_controller' => 'Url Controller',
            'url_path' => 'Url Path',
            'http_method' => 'Http Method',
            'is_core' => 'Is Core',
            'sort_order' => 'Sort Order',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
