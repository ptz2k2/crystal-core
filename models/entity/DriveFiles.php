<?php

namespace crystal\core\models\entity;

use crystal\core\engine\BaseActiveRecord;

/**
 * Class DriveFiles
 * implements the `drive_files` entity model for active record
 *
 * @property string $unique_id
 * @property string $entity_type
 * @property string $identity
 * @property string $file_name
 * @property string $file_type
 * @property string $created_at
 * @property string $updated_at
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\entity
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class DriveFiles extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%crystal_drive_files}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_type', 'identity', 'file_name', 'file_type'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['unique_id'], 'string', 'max' => 64],
            [['entity_type', 'identity', 'file_name'], 'string', 'max' => 255],
            [['file_type'], 'string', 'max' => 100],
            [['unique_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'unique_id' => 'Unique ID',
            'entity_type' => 'Entity Type',
            'identity' => 'Identity',
            'file_name' => 'File Name',
            'file_type' => 'File Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
