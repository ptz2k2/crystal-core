<?php

namespace crystal\core\models\entity;

use crystal\core\engine\BaseActiveRecord;

/**
 * Class Permissions
 * implements the `permissions` entity model for active record
 *
 * @property int $id
 * @property int $role_id
 * @property int $rule_id
 * @property int $has_permission
 * @property string $created_at
 * @property string $updated_at
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\entity
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class Permissions extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%crystal_permissions}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role_id', 'rule_id', 'has_permission'], 'required'],
            [['role_id', 'rule_id', 'has_permission'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => 'Role ID',
            'rule_id' => 'Rule ID',
            'has_permission' => 'Has Permission',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
