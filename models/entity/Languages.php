<?php

namespace crystal\core\models\entity;

use crystal\core\engine\BaseActiveRecord;

/**
 * Class Languages
 * implements the `languages` entity model for active record
 *
 * @property int $id
 * @property string $identity
 * @property string $name
 * @property int $is_active
 * @property int $is_core
 * @property string $created_at
 * @property string $updated_at
 *
 * @package     crystal\core
 * @subpackage  crystal\core\models\entity
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class Languages extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%languages}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identity', 'name', 'is_published', 'is_core'], 'required'],
            [['is_active', 'is_core'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['identity'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identity' => 'Identity',
            'name' => 'Name',
            'is_active' => 'Is Active',
            'is_core' => 'Is Core',
            'created_at' => 'Created At',
        ];
    }
}
