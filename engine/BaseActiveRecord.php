<?php

namespace crystal\core\engine;

use yii\db\ActiveRecord;

use crystal\core\behaviors\DatetimeBehavior;
use crystal\core\behaviors\UniqueIdBehavior;

/**
 * Class BaseActiveRecord
 * implements helper methods to search for records based on multiple criteria
 * implements core behaviors to generate data automatically for entity properties
 * [[unique_id]], [[created_at]], [[updated_at]]
 *
 * @see DatetimeBehavior - generates the created_at and updated_at property datetime value on insert event
 * @see UniqueIdBehavior - generates the unique_id property with a validated generated hash string
 *
 * @package     crystal\core
 * @subpackage  crystal\core\engine
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class BaseActiveRecord extends ActiveRecord
{
    /**
     * {@inheritdoc}
     * implements core behaviors to generate data automatically for entity properties
     * @return array the list of behaviors value
     */
    public function behaviors()
    {
        return [
            [
                'class' => DatetimeBehavior::classname(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['created_at'],
                    self::EVENT_BEFORE_UPDATE => ['updated_at']
                ]
            ],
            UniqueIdBehavior::classname(),
        ];
    }

    /**
     * Find a record based on [[id]] property
     * @param int $id the id value
     * @return ActiveRecord|null active record if the query found result else will return null
     */
    public static function findById( int $id )
    {
        return self::findByAttributes([
            'id' => $id
        ]);
    }

    /**
     * Find a record based on [[unique_id]] property
     * @param string $uniqueId the unique_id value
     * @return ActiveRecord|null active record if the query found result else will return null
     */
    public static function findByUniqueId( string $uniqueId )
    {
        return self::findByAttributes([
            'unique_id' => $uniqueId
        ]);
    }

    /**
     * Find a record based on [[identity]] property
     * @param string $identity the identity value
     * @return ActiveRecord|null active record if the query found result else will return null
     */
    public static function findByIdentity( string $identity )
    {
        return self::findByAttributes([
            'identity' => $identity
        ]);
    }

    /**
     * Find a record based on [[identity]] and [[language_identity]] properties
     * @param string $identity the identity value
     * @param string $languageIdentity the language_identity value
     * @return ActiveRecord|null active record if the query found result else will return null
     */
    public static function findByIdentityAndLanguage( string $identity, string $languageIdentity)
    {
        return self::findByAttributes([
            'identity' => $identity,
            'language_identity' => $languageIdentity
        ]);
    }

    /**
     * Find the first record based on [[sort_order]] property
     * @return ActiveRecord|null active record if the query found result else will return null
     */
    public static function findFirstBySortOrder()
    {
        return static::find()->orderBy(['sort_order' => SORT_ASC])->one();
    }

    /**
     * Find the last record based on [[sort_order]] property
     * @return ActiveRecord|null active record if the query found result else will return null
     */
    public static function findLastBySortOrder()
    {
        return static::find()->orderBy(['sort_order' => SORT_DESC])->one();
    }

    /**
     * Find a record based on multiple attribute criterias
     * @param array $attributes the attribute criterias value for the query
     * @return ActiveRecord|null active record if the query found result else will return null
     */
    public static function findByAttributes( array $attributes )
    {
        return static::find()->where($attributes)->one();
    }

    /**
     * Find all records based on multiple attribute criterias
     * @param array $attributes the attribute criterias value for the query
     * @return array the result of active record query
     */
    public static function findAllByAttributes( array $attributes ) : array
    {
        return static::find()->where($attributes)->all();
    }
}