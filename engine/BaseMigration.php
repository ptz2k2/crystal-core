<?php

namespace crystal\core\engine;

use yii\db\Migration;

/**
 * Class BaseMigration
 * extends the yii migration class to implement the migration base configuration and helper methods
 *
 * @package     crystal\core
 * @subpackage  crystal\core\engine
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class BaseMigration extends Migration
{
    const ENGINE_SET = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
}