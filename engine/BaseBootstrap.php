<?php

namespace crystal\core\engine;

use yii\base\BootstrapInterface;
use yii\console\Application as ConsoleApplication;

use crystal\core\CrystalApplication as WebApplication;
use crystal\core\helpers\ArrayHelper;
use crystal\core\helpers\BootstrapHelper;

/**
 * Class BaseBootstrap
 * the base bootstrap class that implements the BootstrapInterface and implements the bootstrap method which
 * injects the configuration based on Application instance type.
 * if the application instance is WebApplication the bootstrap will call the injectWebApplicationConfiguration method
 * if the application instance is ConsoleApplication the bootstrap will call the injectConsoleApplicationConfiguration method
 *
 * @method injectWebApplicationConfiguration injects into the migrations, components, urlRules configurations into
 * the application instance configuration
 * this method also initializes the requried components if the component configuration bootstrap value is true
 *
 * @method injectConsoleApplicationConfiguration is empty since the base bootstrap does not implements anything for
 * console application
 *
 * @package     crystal\core
 * @subpackage  crystal\core\engine
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
abstract class BaseBootstrap implements BootstrapInterface
{
    /**
     * @var array the migration list that will be injected into application migrations configuration
     */
    private $migrations = [];

    /**
     * @var array the components list that will be injected into application components configuration
     */
    private $components = [];

    /**
     * @var array the url rules list that will be injected into application url manager rules configuration
     */
    private $urlRules = [];

    /**
     * Initialize the application bootstraping process by checking the type of the application instance
     * if the instance is WebApplication the bootstrap will call injectWebConfigurations method
     * if the instance is ConsoleApplication the bootstrap will call injectConsoleConfigurations method
     */
    public function bootstrap( $application ) : void
    {
        if ( $application instanceof WebApplication ) {
            $this->injectWebApplicationConfigurations($application);
        }

        if ( $application instanceof ConsoleApplication ) {
            $this->injectConsoleApplicationConfigurations($application);
        }
    }

    /**
     * Injects the configurations to the current web application instance
     * the method will inject the migrations, component and url rules if the properties are not empty.
     * this method will also initialize components if the component configuration `bootstrap` value is true.
     * Note that if the application configuration is already cached then this method will just initialize the components
     * Note if you override this method please call the parent::init() method in as last step
     * @param WebApplication $application the web application instance
     */
     public function injectWebApplicationConfigurations( WebApplication $application ) : void
     {
         if ( !$application->isCachedConfiguration() ) {
             if ( !empty($this->urlRules) ) {
                 BootstrapHelper::injectUrlRules($application, $this->urlRules);
             }

             if ( !empty($this->components) ) {
                 foreach ( $this->components as $componentIdentity => $componentConfiguration ) :

                     if ( isset($componentConfiguration['bootstrap']) ) {
                         unset($componentConfiguration['bootstrap']);
                     }

                     BootstrapHelper::injectComponent($application, $componentIdentity, $componentConfiguration);
                 endforeach;
             }
         }

         if ( !empty($this->components) ) {
             foreach ( $this->components as $componentIdentity => $componentConfiguration) :

                 if ( isset($componentConfiguration['bootstrap']) && $componentConfiguration['bootstrap'] === true ) {
                     \Yii::$app->{$componentIdentity};
                 }

             endforeach;
         }
     }

    /**
     * Injects the configuration to the current console application instance
     * @param ConsoleApplication $application the console application instance
     */
    public function injectConsoleApplicationConfigurations( ConsoleApplication $application ) : void
    {
        if ( !empty($this->migrations) ) {
            foreach ( $this->migrations as $migrationPath ) :
                BootstrapHelper::injectMigrationPath($application, $migrationPath);
            endforeach;
        }
    }

    /**
     * Add's a migration to the migrations list
     * @param string $migrationPath the migration path of the migration
     */
    protected function addMigration( string $migrationPath ) : void
    {
        $this->migrations[] = $migrationPath;
    }

    /**
     * Add's a component to the components list
     * @param string $identity the identity of the component
     * @param array $configuration the component configuration
     */
    protected function addComponent( string $identity, array $configuration ) : void
    {
        $this->components[$identity] = $configuration;
    }

    /**
     * Add's url rules to the url rules list
     * @param array $urlRules the url rules value
     */
    protected function addUrlRules( array $urlRules ) : void
    {
        $this->urlRules = array_merge($this->urlRules, $urlRules);
    }

}