<?php

namespace crystal\core\engine;

use yii\base\BaseObject;

use crystal\core\engine\exceptions\ConfigurationNotFoundException;
use crystal\core\engine\interfaces\ComponentInterface;

/**
 * Class BaseComponent
 * implements ComponentIterface `getCache` and `setCache` methods to get and generate the component cache data.
 * extends the init method to check if the current component has a COMPONENT_CACHE_NAME constant defined,
 * if yes the method will retrieve the current component cache data and store it in $cache property
 *
 * @property array|null $cache the cache data retrieved from cache storage, will be populated on component init method
 * @property array|null $session the session data retireved from session storage, will be populated on component init method
 *
 * @package     crystal\core
 * @subpackage  crystal\core\engine
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
abstract class BaseComponent extends BaseObject implements ComponentInterface
{
    /**
     * @var array|null the component cache data, will be populated with the cache data on component init method
     */
    private $cache;

    /**
     * @var array|null the component session data, will be populated with the session data on component init method
     */
    private $session;

    /**
     * {@inheritdoc}
     * verifies if the component has a constant COMPONENT_CACHE_NAME defined, if yes then the method will retrieve
     * the cache data of the current component and will populate the $cache property with the returned value
     * verifies if the component has a constant COMPONENT_SESSION_NAME defined, if yes then the method will retrieve
     * the session data of the current component and will populate the $session property with the returned value
     */
    public function init()
    {
        if( defined($this::className() . '::COMPONENT_CACHE_NAME') ) {
            $this->cache = $this->getCache();
        }

        if( defined($this::className() . '::COMPONENT_SESSION_NAME') ) {
            $this->session = $this->getSession();
        }

        parent::init();
    }

    /**
     * {@inheritdoc}
     * @throws ConfigurationNotFoundException if the requested configuration was not found
     */
    public function getConfig(string $identity, string $property)
    {
        if( $this->{$property} !== null ) {
            return $this->{$property};
        }

        $configuration = \Yii::$app->configuration->getGlobalConfiguration($identity);
        $this->{$property} = $configuration;

        return $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function getCache() : ? array
    {
        if( $this->cache !== null ) {
            return $this->cache;
        }

        $cache = \Yii::$app->cache->get(static::COMPONENT_CACHE_NAME);

        if( $cache === false ) {
            return null;
        }

        $this->cache = $cache;
        return $cache;
    }

    /**
     * {@inheritdoc}
     */
    public function setCache( array $value ) : void
    {
        \Yii::$app->cache->set(static::COMPONENT_CACHE_NAME, $value);
    }

    /**
     * {@inheritdoc}
     */
    abstract function generateCacheData() : array;

    /**
     * {@inheritdoc}
     */
    public function getSession(): ? array
    {
        if( $this->session !== null ) {
            return $this->session;
        }

        $session = \Yii::$app->session->get(static::COMPONENT_SESSION_NAME);

        $this->session = $session;
        return $session;
    }

    /**
     * {@inheritdoc}
     */
    public function setSession( array $value ) : void
    {
        \Yii::$app->session->set(static::COMPONENT_SESSION_NAME,$value);
    }
}