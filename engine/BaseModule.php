<?php

namespace crystal\core\engine;

use Yii;
use yii\base\Module;

use Symfony\Component\Yaml\Yaml;

use crystal\core\engine\interfaces\ModuleInterface;
use crystal\core\engine\exceptions\ConfigurationNotFoundException;
use crystal\core\engine\exceptions\ModuleEntityNotFoundException;
use crystal\core\models\repository\ModulesRepository;
use crystal\core\helpers\BootstrapHelper;
use crystal\core\helpers\DataFormatterHelper;

/**
 * Class Module
 * extends the base module class to implement custom methods to handle the application modules initialization
 *
 * @package     crystal\core
 * @subpackage  crystal\core\engine
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class BaseModule extends Module implements ModuleInterface
{
    const CONFIGURATION_FILENAME = 'configuration.yaml';

    /**
     * @var ModulesRepository|null the module entity data, will be populated on module init method
     */
    private $moduleEntity;

    /**
     * @var array|null the module configuration data, will be populated on module init method
     */
    private $moduleConfiguration;

    /**
     *
     */
    public function init()
    {
        $this->moduleEntity = self::getModuleEntity($this->id);
        $this->moduleConfiguration = self::getModuleConfiguration($this->moduleEntity);

        $this->injectModuleTranslations();

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public static function getModuleEntity( string $id ): ModulesRepository
    {
        $moduleEntity = ModulesRepository::findByIdentity($id);

        if( !$moduleEntity ) {
            throw new ModuleEntityNotFoundException(500,
                Yii::t('crystal.exceptions', 'The module entity was not found for module `{0}`', [
                    $id
                ])
            );
        }

        return $moduleEntity;
    }

    /**
     * {@inheritdoc}
     */
    public static function getModuleConfiguration( ModulesRepository $moduleEntity ): array
    {
        $parsedBasePath = DataFormatterHelper::parseBasePath($moduleEntity->base_path);
        $configurationPath = Yii::getAlias($parsedBasePath) . self::CONFIGURATION_FILENAME;

        if( !file_exists($configurationPath) ) {
            throw new ConfigurationNotFoundException(500,
                Yii::t('crystal.exceptions', 'The configuration for module `{0}` was not found in path {1}',[
                    $moduleEntity->identity,
                    $configurationPath
                ])
            );
        }

        return Yaml::parseFile($configurationPath);
    }

    /**
     * {@inheritdoc}
     */
    public function injectModuleTranslations(): void
    {
        if( isset($this->moduleConfiguration['translations']) && !empty($this->moduleConfiguration['translations']) ) {
            //var_dump($this->moduleConfiguration['translations']); die;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isInstalled(): bool
    {
        return (bool) $this->moduleEntity->is_installed;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled(): bool
    {
        return (bool) $this->moduleEntity->is_enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function isCore(): bool
    {
        return (bool) $this->moduleEntity->is_core;
    }
}