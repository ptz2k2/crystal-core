<?php

namespace crystal\core\engine\interfaces;

/**
 * Interface RepositoryCacheInterface
 * this interface should be implemented by a class providing a repositry that requires cache storage
 *
 * @package     crystal\core
 * @subpackage  crystal\core\engine\interfaces
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
interface RepositoryCacheInterface
{
    /**
     * Retrieves the cache data from the cache storage
     * @return array|null the cache value, null if the cache not exists
     */
    static function getCache() : ? array;

    /**
     * Sets the cache data with the value sent in parameter
     * @param array $value the value what you want to store
     */
    static function setCache( array $value ) : void;

    /**
     * Generates the cache data for the repository that will be stored in cache storage
     * @return array the generated cache data for the repository
     */
    static function generateCacheData() : array;
}