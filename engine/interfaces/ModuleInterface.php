<?php

namespace crystal\core\engine\interfaces;

use crystal\core\models\repository\ModulesRepository;

/**
 * Interface ModuleInterface
 * this interface should be implemented by a class providing a module for the application
 *
 * @package     crystal\core
 * @subpackage  crystal\core\engine\interfaces
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
interface ModuleInterface
{
    /**
     * Retrieves the current module entity from the database
     * @param string $identity the current module id
     * @return ModulesRepository the current module entity data
     * @throws \crystal\core\engine\exceptions\ModuleEntityNotFoundException if the module entity is not found
     */
    static function getModuleEntity( string $id ) : ModulesRepository;

    /**
     * Retrieves the current module configuration from yaml file
     * @param ModulesRepository $moduleEntity the current module entity data
     * @return array the configuration value
     * @throws \crystal\core\engine\exceptions\ConfigurationNotFoundException if the configuration file not exists
     */
    static function getModuleConfiguration( ModulesRepository $moduleEntity ) : array;

    /**
     * Inject the current module translation sources into the application instance
     */
    function injectModuleTranslations() : void;

    /**
     * Returns if the current module is installed or not
     * @return bool whatever the module is installed or not
     */
    function isInstalled() : bool;

    /**
     * Returns if the current module is enabled or not
     * @return bool whatever the module is enabled or not
     */
    function isEnabled() : bool;

    /**
     * Returns if the current module is a core module or not
     * @return bool whatever the module is core or not
     */
    function isCore(): bool;

}