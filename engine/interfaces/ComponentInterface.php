<?php

namespace crystal\core\engine\interfaces;

/**
 * Interface ComponentInterface
 * this interface should be implemented by a class providing a component for the application
 *
 * @package     crystal\core
 * @subpackage  crystal\core\engine\interfaces
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
interface ComponentInterface
{
    /**
     * Retrieve the configuration based on configuration identity
     * if the requested property is not null the method will return the property value
     * @param string $identity the identity value of the configuration
     * @param string $property the class property where to store the configuration value
     * @return mixed the configuration value
     */
    function getConfig( string $identity, string $property);

    /**
     * Retrieves the cache data from the cache storage
     * @return array|null the cache value, null if the cache not exists
     */
    function getCache() : ? array;

    /**
     * Sets the cache data with the value sent in parameter
     * @param array $value the value what you want to store
     */
    function setCache( array $value ) : void;

    /**
     * Generates the cache data for the component and stores it in the cache storage
     * @return array the generated cache data for the component
     */
    function generateCacheData() : array;

    /**
     * Retrieves the session data from session storage
     * @return array|null the session value, null if the session not exists
     */
    function getSession() : ? array;

    /**
     * Sets the session data with the value sent in parameter
     * @param array $value the value what you want to store
     */
    function setSession( array $value) : void;

}