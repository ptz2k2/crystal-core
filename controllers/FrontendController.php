<?php

namespace crystal\core\controllers;

/**
 * Class FrontendController
 * implements the frontend application theme based on configuration data
 *
 * @package     crystal\core
 * @subpackage  crystal\core\controllers
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class FrontendController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function beforeAction( $beforeAction ): bool
    {
        \Yii::$app->theme->setTheme(\Yii::$app->configuration->getGlobalConfiguration('crystal.theme.frontend'));
        return parent::beforeAction($beforeAction);
    }
}