<?php

namespace crystal\core\controllers;

use yii\base\ActionEvent;
use yii\web\Controller as BaseController;

/**
 * Class Controller
 * extends the yii base controller to implement crystal core controllers custom functionality and helper methods
 *
 * @package     crystal\core
 * @subpackage  crystal\core\controllers
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class Controller extends BaseController
{
    /**
     * @var string will be populated with the current action path
     */
    public $pageIdentity;

    /**
     * {@inheritdoc}
     * override init method to retrieve the request language paremeter, and update the current language
     * if the language is not equal with the current language and it is valid
     * @see \crystal\core\components\TranslationComponent::setLanguage()
     */
    public function init()
    {
        /**$languageCode = \Yii::$app->request->get('language');

        if ( $languageCode !== null && $languageCode !== \Yii::$app->translation->getCurrentLanguage() ) {
            \Yii::$app->translation->setLanguage($languageCode);
        }**/

        return parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function beforeAction( $action ) : bool
    {
        $this->pageIdentity = $this->_generatePageIdentity();

        $event = new ActionEvent($action);
        $this->trigger(self::EVENT_BEFORE_ACTION, $event);
        return $event->isValid;
    }

    /**
     * {@inheritdoc}
     */
    public function render( $view, $params = [] )
    {
        return parent::render($view, $params);
    }

    /**
     * Generates the page identity for the current action from ModuleId, ControllerId and ActionId
     * @return string the generated page identity
     */
    private function _generatePageIdentity() : string
    {
        return $this->module->id . '-' . $this->id . '-' . $this->action->id;
    }
}