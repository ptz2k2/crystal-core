<?php

namespace crystal\core\helpers;

/**
 * Class DataGeneratorHelper
 * implements helper methods to generate multiple type of values for the application
 *
 * @package     crystal\core
 * @subpackage  crystal\core\helpers
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class DataGeneratorHelper
{
    const SETTING_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * Generates unique id hash for the the entity with `$className`
     * this class also validates in the entity table does not have the generated hash else will generate new one
     * until finds an hash that is not found in the entity table
     * @param string $className the clasname of the entity model
     * @return string the generated unique id hash
     */
    public static function generateUniqueId( string $className ) : string
    {
        do {
            $uniqueId = \Yii::$app->security->generateRandomString(64);
            $isUniqueId = $className::findByUniqueId($uniqueId);

        } while( $isUniqueId );

        return $uniqueId;
    }

    /**
     * Generates the sort order number for the requested entity
     * this method checks if the requested entity class has records and returns the next sort_order number
     * else the method will return 1 as sort_order
     * @param string $entityClass the entity class name
     * @return int the next sort order number value
     */
    public static function generateSortOrder( $entityClass ) : int
    {
        $isRecord = $entityClass::findLastBySortOrder();

        if( !$isRecord ) {
            return 1;
        }

        return $isRecord->sort_order++;
    }

    /**
     * Generates the current datetime with the application datetime format
     * @return string the generated datetime
     */
    public static function currentDatetime() : string
    {
        return date(self::SETTING_DATE_FORMAT);
    }

    /**
     * Generates the current timestamp
     * @return int the generated timestamp
     */
    public static function currentTimestamp() : int
    {
        $dateTime = self::currentDatetime();
        return strtotime($dateTime);
    }

    /**
     * Generate custom datetime with date offset calculation
     * @param string $dateRange date calculation range ex:( +7 days )
     * @return string the generated datetime
     */
    public static function generateDatetime( string $dateRange ) : string
    {
        $dateRange = strtotime($dateRange);
        return date(self::SETTING_DATE_FORMAT, $dateRange);
    }

    /**
     * Generate custom timestamp with custom date offset calculation
     * @param string $dateRange date calculation range ex:( +7 days )
     * @return int the generated timestamp
     */
    public static function generateTimestamp( string $dateRange ) : int
    {
        $date = self::generateDatetime($dateRange);
        return strtotime($date);
    }
}