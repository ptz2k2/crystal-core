<?php

namespace crystal\core\helpers;

use crystal\core\engine\BaseModule;
use crystal\core\helpers\DataFormatterHelper;

/**
 * Class BootstrapHelper
 * implements helper methods to inject configuration data to the application instance
 *
 * @package     crystal\core
 * @subpackage  crystal\core\helpers
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class BootstrapHelper
{
    /**
     * Inject a component to the application instance configuration
     * @param mixed $application the application instance
     * @param string $componentIdentity the component identity value
     * @param array $configuration the component configuration value
     * @return mixed the updated application instance
     * @see https://www.yiiframework.com/doc/guide/2.0/en/structure-application-components
     */
    public static function injectComponent( $application, string $componentIdentity, array $configuration )
    {
        $applicationComponents = $application->components;
        $applicationComponents[$componentIdentity] = $configuration;

        $application->components = $applicationComponents;
        return $application;
    }

    /**
     * Injects a migration path to the application instance configuration
     * @param mixed $application the application instance
     * @param string $migrationPath the migration path
     * @return mixed the updated application instance
     * @see https://www.yiiframework.com/doc/guide/2.0/en/db-migrations
     */
    public static function injectMigrationPath( $application, string $migrationPath )
    {
        $application->params['yii.migrations'][] = $migrationPath;
        return $application;
    }

    /**
     * Injects the url rules to application instance configuration
     * @param mixed $application the application instance
     * @param array $urlRules the url rules
     * @return mixed the updated application instance
     */
    public static function injectUrlRules( $application, array $urlRules )
    {
        $application->getUrlManager()->addRules($urlRules,false);
        return $application;
    }

    /**
     * Injects a module to the application instance configuration
     * @param mixed $application the application instance
     * @param string $identity the module identity
     * @param array $configuration the module configuration
     * @return mixed the application instance
     * @see https://www.yiiframework.com/doc/guide/2.0/en/structure-modules#modules for more information about module configuration
     */
    public static function injectModule( $application, string $moduleIdentity, array $configuration )
    {
        $application->setModule( $moduleIdentity, $configuration);
        return $application;
    }

    /**
     * Injects all modules sent in parameter to the application instance configuration
     * if the module has components the method will inject the module components to
     * @param mixed $application the application instance
     * @param array $modules the modules data
     * @return mixed the updated application instance
     */
    public static function injectModules( $application, array $modules )
    {
        if( empty($modules) ) {
            return $application;
        }

        foreach( $modules as $module ) :
            $moduleBasePath = DataFormatterHelper::parseBasePath($module->base_path);
            $moduleConfiguration = BaseModule::getModuleConfiguration($module);

            if( isset($moduleConfiguration['components']) && !empty($moduleConfiguration['components']) ) {
                foreach ($moduleConfiguration['components'] as $componentIdentity => $componentConfiguration ) {
                    self::injectComponent($application, $componentIdentity, $componentConfiguration);
                }
            }

            self::injectModule($application, $module->identity, $moduleConfiguration['module']);
        endforeach;

        return $application;
    }
}