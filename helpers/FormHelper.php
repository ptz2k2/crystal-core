<?php

namespace crystal\core\helpers;

/**
 * Class FormHelper
 * implements helper methods to manage the application form data
 *
 * @package     crystal\core
 * @subpackage  crystal\core\helpers
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class FormHelper
{
    /**
     * Generates a json response with the form status and messages
     * @param string $formStatus the current form status [danger,warning,success]
     * @param array $messages the array of messages what you want to send
     * @param bool $modelErrors if the messages are sent from $model->getErrors()
     * @return string the json_encoded response
     */
    public static function makeAjaxResponse( string $formStatus, array $messages, bool $modelErrors = false ) : string
    {
        $response = [
            'status' => $formStatus,
            'messages' => $messages,
        ];

        if ( $modelErrors === true ) {
            $response['messages'] = self::formatModelErros($messages);
        }

        return json_encode($response);
    }

    /**
     * Retrieves the errors for each field from the field array
     * @param array $modelErrors the model errors
     * @return array the formatted model errors array
     */
    public static function formatModelErros( array $modelErrors ) : array
    {
        $formattedErrors = [];

        foreach ( $modelErrors as $modelField => $modelFieldErrors ) :
            foreach( $modelFieldErrors as $modelFieldError ) :
                $formattedErrors[] = $modelFieldError;
            endforeach;
        endforeach;

        return $formattedErrors;
    }
}
