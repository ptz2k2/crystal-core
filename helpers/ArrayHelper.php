<?php

namespace crystal\core\helpers;

use yii\helpers\ArrayHelper as BaseArrayHelper;

/**
 * Class ArrayHelper
 * extends the yii framework array helper to implement additional methods to validate and manage array data
 *
 * @package     crystal\core
 * @subpackage  crystal\core\helpers
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class ArrayHelper extends BaseArrayHelper
{
    /**
     * Verifies the value if is a serialized array
     * @param string $value the value what you want to validate
     * @return bool whatever the value is a serialized or not
     */
    public static function isSerialized( string $value ) : bool
    {
        if ( !is_string($value) ) {
            return false;
        }

        $value = trim($value);

        if ( 'N;' == $value ) {
            return true;
        }

        if ( !preg_match('/^([adObis]):/', $value, $badions) ) {
            return false;
        }

        switch ( $badions[1] ) {
            case 'a' :
            case 'O' :
            case 's' :
                if ( preg_match("/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $value) ) {
                    return true;
                }
                break;
            case 'b' :
            case 'i' :
            case 'd' :
                if ( preg_match("/^{$badions[1]}:[0-9.E-]+;\$/", $value) ) {
                    return true;
                }
                break;
        }

        return false;
    }
}