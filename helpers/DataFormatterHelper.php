<?php

namespace crystal\core\helpers;

/**
 * Class DataFormatterHelper
 * implements helper methods to format multiple type of values for the application
 *
 * @package     crystal\core
 * @subpackage  crystal\core\helpers
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class DataFormatterHelper
{
    /**
     * @var array the html options supported by data formatter helper
     */
    private static $htmlOptions = array(
        'id' => '',
        'class' => '',
        'attributes' => array(),
    );

    /**
     * Parses the module base path and check if the $basePath has directory separator ending
     * if is directory separator at the end of the string the method will return the parsed $basePath
     * if isn't directory separator at the end of the string the method will add directory separator to the parsed @basePath
     * @param string $basePath the path what you want to parse
     * @return string the parsed base path
     */
    public static function parseBasePath( string $path ) : string
    {
        return $path = rtrim($path, '/') . '/';
    }

    /**
     * Generates the formatted html options array data
     *
     * @param string $value the serialized html_options data
     * @return array the generated html options data
     */
    public static function generateHtmlOptions( string $value ) : array
    {
        $htmlOptionsData = unserialize($value);

        if( empty($htmlOptionsData) ) {
            return self::$htmlOptions;
        }

        $formattedOptions = self::$htmlOptions;

        foreach($htmlOptionsData as $optionKey => $optionValue ) :
            $formattedOptions[$optionKey] = $optionValue;
        endforeach;

        var_dump($htmlOptionsData);
    }
}