<?php

namespace crystal\core;

use yii\console\Application as ConsoleApplication;

use crystal\core\CrystalApplication as WebApplication;
use crystal\core\engine\BaseBootstrap;
use crystal\core\models\repository\ModulesRepository;
use crystal\core\models\repository\RoutesRepository;
use crystal\core\helpers\BootstrapHelper;

/**
 * Class Bootstrap
 * injects the application enabled modules into the application instance.
 * injects the core migration paths and the all the application url routes.
 *
 * @package     crystal\core
 * @subpackage  crystal\core\engine
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites
 */
class Bootstrap extends BaseBootstrap
{
    /**
     * {@inheritdoc}
     */
    public function injectWebApplicationConfigurations( WebApplication $application ) : void
    {
       return;
        if( !$application->isInstalled() ) {
            return;
        }

        if ( !$application->isCachedConfiguration() ) {
            $this->injectModules($application);
            $this->addUrlRules(RoutesRepository::findAllFormatted());
        }

        parent::injectWebApplicationConfigurations($application);

        $application->maintenance;
        $application->routing;
        $application->translation;
    }

    /**
     * {@inheritdoc}
     */
    public function injectConsoleApplicationConfigurations(ConsoleApplication $application): void
    {
        $this->addMigration('@app/migrations/insert');
        $this->addMigration('@vendor/ptz2k2/crystal-core/migrations');
        $this->addMigration('@vendor/ptz2k2/crystal-core/migrations/insert');

        parent::injectConsoleApplicationConfigurations($application);
    }

    /**
     * Injects the application enabled modules into the current WebApplication instance
     * @param WebApplication $application the application instance
     */
    protected function injectModules( WebApplication $application ) : void
    {
        $enabledModules = ModulesRepository::findAllEnabled();
        BootstrapHelper::injectModules($application, $enabledModules);
    }
}