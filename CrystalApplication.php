<?php

namespace crystal\core;

use yii\web\Application;

use crystal\core\components\ConfigurationComponent;
use crystal\core\components\MaintenanceComponent;
use crystal\core\components\RoutingComponent;
use crystal\core\components\SessionComponent;
use crystal\core\components\ThemeComponent;
use crystal\core\components\TranslationComponent;

/**
 * Class CrystalApplication
 * extends the Application class to set the application base components and to implement custom configuration options.
 *
 * @property array $crystal contains the crystal core required application configurations
 * 
 * @package     crystal\core
 * @version     1.0.0
 * @since       1.0.0
 * @author      Tamas Palecian <tamas.palecian@nucleuswebs.com>
 * @link        http://www.nucleuswebs.com
 * @copyright   Copyright (c) 2018 Nucleus websites 
 */
class CrystalApplication extends Application
{
    public $is_installed;
    public $is_cached_configuration;

    /**
     * {@inheritdoc}
     */
    public function init() : void
    {
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function coreComponents() : array
    {
        return array_merge(parent::coreComponents(), [
            'configuration' => ConfigurationComponent::classname(),
            'maintenance' => MaintenanceComponent::className(),
            'routing' => RoutingComponent::className(),
            'session' => SessionComponent::classname(),
            'theme' => ThemeComponent::className(),
            //'translation' => TranslationComponent::classname()
        ]);

        return parent::coreComponents();
    }

    public function isProduction()
    {
        return false;
    }
}
